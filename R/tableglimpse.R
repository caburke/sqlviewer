tableglimpse <- function(dbsrc, n = 5) {

  assertthat::assert_that('src_sql' %in% class(dbsrc))

  tables <- dplyr::db_list_tables(dbsrc$con)

  ui <- miniPage(
    gadgetTitleBar('SQL Viewer'),
    miniContentPanel(
      selectInput(inputId = 'tables',
                  label = 'Tables',
                  choices = tables,
                  selected = tables[1]),
      DT::dataTableOutput('dt')
    )
  )

  server <- function(input, output, session) {

    selectedTable <- reactive({
      dplyr::tbl(dbsrc, input$tables)
    })

    output$dt <- DT::renderDataTable({
      df <- dplyr::collect(selectedTable(), n=n)
      cols <- colnames(df)
      types <- unlist(lapply(df, class))
      tibble::tibble(
        Columns=cols,
        Types=types,
        Val1 = unlist(df[1,]),
        Val2 = unlist(df[2,])
      )
    })

  }

  runGadget(ui, server, viewer=dialogViewer('SQL Viewer'))
}
